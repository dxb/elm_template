module Main exposing (main)

import Html exposing (..)
import Html.Attributes exposing (class, href)
import Browser

-- COLORS

purple = "#71454E"

darkGray = "#2A2B2A"
lightGray = "#7F7979"

lightBlue = "#768699"
darkBlue = "#313B61"

primary = darkBlue
secondary = lightGray
bgColor = darkGray

-- MAIN

main = Browser.element
  { init = init
  , view = view
  , update = update
  , subscriptions = subscriptions
  }

-- MODEL

type alias Model =
  { notebookCount: Int
  }

-- type NotebookSize = NotebookSize PreferStandard MetricSizeMm MetricSizeMm

-- type PreferStandard = Ansi | Metric

-- type MetricSizeMm = Int

-- INIT

init : () -> (Model, Cmd Msg)
init _ = ({ notebookCount = 70 }, Cmd.none)

filterCriteria : List (String, Msg)
filterCriteria =
  [ ("Size", {})
  , ("Layout / Rule Style", {})
  ]

-- UPDATE

type alias Msg = {}

update : Msg -> Model -> (Model, Cmd Msg)
update _ model = (model, Cmd.none)

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions _ = Sub.none

-- VIEW

view : Model -> Html Msg
view model =
  div []
    [ header, body model ]

centered : List (Html Msg) -> Html Msg
centered els = div [ class "centered" ] els

header = div [ class "header" ]
  [ centered [
      h1 []
        [ text "Find the right notebook" ]
    ]
  ]

body : Model -> Html Msg
body model = div [ class "body" ]
  [ centered
    [ sectionHeading "Let's get started"
    , p []
        [ text
            ("Our catalog has "
            ++ String.fromInt model.notebookCount
            ++ " notebooks."
            )
        ]
    , p []
        [ text "Let's narrow that down." ]
    , p []
        [ text "I want to start searching by..."]
    , div [] (List.map criteriaButton filterCriteria)
    ]
  ]

sectionHeading : String -> Html Msg
sectionHeading str = h2 [ class "heading" ] [ text str ]

criteriaButton : (String, Msg) -> Html Msg
criteriaButton (name, _) = a [ class "criteria-button", href "#" ]
  [ div [ class "criteria-circle" ] []
  , div [ class "criteria-info" ] [ text name ]
  ]
