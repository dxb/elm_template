#!/bin/sh
mkdir -p dist
elm make src/Main.elm --output dist/main.js
sass src/main.scss:dist/main.css
cp -r ./public/. ./dist/
